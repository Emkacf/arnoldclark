(function () {
  'use strict';


/**
 * @ngdoc overview
 * @name arnoldClarkApp
 * @description
 * # arnoldClarkApp
 *
 * Main module of the application.
 */
angular
  .module('arnoldClarkApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial',
    'ui.bootstrap',
    'bootstrapLightbox'
  ])
  .constant('_', window._)
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
})();
