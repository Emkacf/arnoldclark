'use strict';

/**
 * @ngdoc service
 * @name arnoldClarkApp.imageService
 * @description
 * # imageService
 * Service which returns object with images url
 */
angular.module('arnoldClarkApp')
  .factory('imageService', function () {
    var self = this;

    self.registration = '';
    self.stockReference = '';
    self.sizeArray = ['350','800'];
    self.cameraArray = ['f', 'i', 'r', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

    function getReversedRegistration() {
      return _
        .chain(self.registration)
        .toUpper()
        .toArray()
        .reverse()
        .value();
    }

    function getControlNumber() {
      return  _
        .chain(self.stockReference)
        .toUpper()
        .value()
        .charAt(8);
    }

    function getObfuscatedStockReference() {
      var controlNumber = getControlNumber();
      var reversedRegistration = getReversedRegistration();

      return _
        .chain(self.stockReference)
        .toUpper()
        .take(self.registration.length)
        .zip(reversedRegistration)
        .push(controlNumber)
        .flatten()
        .compact()
        .join('')
        .value();
    }
    /**
     * @ngdoc function
     * @name getImages
     *@description
     * Search image service obtains images list for exact car
     * @param {string} stockReference stock reference code
     * @param {string} registrationPlate registration plate code
     * @returns {object} resolve with fetched data, or fails with error description.
     */
    function getImages(stockReference, registration){
      if(_.isEmpty(stockReference)){ throw new Error('Stock Reference is required'); }
      if(_.isEmpty(registration)){ throw new Error('Registration Plate is required'); }
      self.stockReference = stockReference;
      self.registration = registration;

      var baseUrl = 'https://vcache.arnoldclark.com/imageserver';
      var obfuscatedStockReference = getObfuscatedStockReference();
      var images = [];

      _.forEach(self.cameraArray, function(camera) {
        var obj = {};
        _.forEach(self.sizeArray, function(size) {
          obj[size] = [baseUrl, obfuscatedStockReference, size, camera].join('/');
        });
        images.push(obj);
      });

      return images;

    }

    return {
      getImages: getImages
    };
  });
