'use strict';

/**
 * @ngdoc directive
 * @name arnoldClarkApp.directive:carGallery
 * @description
 * # carGallery directive is responsible for showing gallery of photos
 */
angular.module('arnoldClarkApp')
  .directive('carGallery', function () {
    return {
      templateUrl : 'views/gallery.html',
      transclude: true,
      scope: {
        thumbnail : '=',
        image : '=',
        index : '=',
        all : '='
      },
      restrict: 'E',
      controller: function($scope, $mdMedia, $mdDialog){

        console.log($scope.image);

        $scope.openLightboxModal = function (ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                scope: $scope,
                preserveScope: true,
                templateUrl: 'views/gallery-dialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen
              });

            $scope.$watch(function() {
              return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
              $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        $scope.cancel = function() {
          $mdDialog.cancel();
        };

      }
    };
  });
