'use strict';

/**
 * @ngdoc function
 * @name arnoldClarkApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Responsible for main page
 */
angular.module('arnoldClarkApp')
  .controller('MainCtrl', function (imageService) {
    var vm = this;

    vm.stockReference = '';
    vm.registrationPlate = '';
    vm.images = [];
    vm.show = false;

    /**
     * @ngdoc function
     * @name searchImages
     *@description
     * Search image invoke imageService.getImages to obtain object with list of images for exact car
     * @param {string} stockReference stock reference code
     * @param {string} registrationPlate registration plate code
     * @returns {object} resolve with fetched data, or fails with error description.
     */
    vm.searchImages = function(){
      vm.images = imageService.getImages(vm.stockReference,vm.registrationPlate);
      vm.show = true;
    };

  });
