'use strict';

describe('Service: imageService', function () {

  // load the service's module
  beforeEach(module('arnoldClarkApp'));

  // instantiate service
  var imageService;
  beforeEach(inject(function (_imageService_) {
    imageService = _imageService_;
  }));

  it('should return images object for given stock reference and registration plate', function () {
    var stockReference = 'ARNBT-U-41160';
    var registrationPlate = 'WR14HFT';

    expect((imageService.getImages(stockReference,registrationPlate)).length).toBe(12);
  });

});
