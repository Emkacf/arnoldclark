'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('arnoldClarkApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should get images for given stock reference and registration plate', function () {
    MainCtrl.stockReference = 'ARNBT-U-41160';
    MainCtrl.registrationPlate = 'WR14HFT';

    MainCtrl.searchImages();

    expect(MainCtrl.images.length).toBe(12);
  });

});
